# PhotoOrganizer
Simple project to organize .jpg files into folders sorted by year and month.

 ## Building
 Requirements:
  - [Git](https://git-scm.com/downloads)
  
 Assuming that's all installed and ready to go, you can proceed with the following:
  
1. Clone the repository: `git clone --recursive https://gitlab.com/Madoba/photosorganizer`
2. Run `scripts/Win-GenProjects.bat` - This will generate makefiles for the project
4. Run `scripts/Compile-release.bat` - This will compile the project to bin\Release-windows-x86_64\PhotoOrganizer folder
