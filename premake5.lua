workspace "PhotoOrganizer"
	architecture "x86_64"
	startproject "PhotoOrganizer"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}
	
	flags
	{
		"MultiProcessorCompile"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include directories relative to root folder (solution directory)
IncludeDir = {}
IncludeDir["GoogleTest"] = "PhotoOrganizer/vendor/googletest"

group "Dependencies"
	include "PhotoOrganizer/vendor/googletest"

group ""

project "PhotoOrganizer"
	location "PhotoOrganizer"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"%{prj.name}/src/**.hpp",
		"%{prj.name}/src/**.cpp",
		"%{prj.name}/tests/**.cpp"
	}

	defines
	{
		"_CRT_SECURE_NO_WARNINGS"
	}

	includedirs
	{
		"%{prj.name}/src",
		"%{IncludeDir.GoogleTest}/googletest/include",
		"%{IncludeDir.GoogleTest}/googlemock/include"
	}

	links 
	{ 
		"googletest"
	}

	filter "system:windows"
		systemversion "latest"

	filter "configurations:Debug"
		defines "PO_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "PO_RELEASE"
		runtime "Release"
		optimize "on"

	filter "configurations:Dist"
		defines "PO_DIST"
		runtime "Release"
		optimize "on"