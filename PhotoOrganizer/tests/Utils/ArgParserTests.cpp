#include <vector>
#include <string>
#include <iostream>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wuseless-cast"
#include "gtest/gtest.h"
#pragma GCC diagnostic pop

#include "Utils/ArgParser.hpp"

const char** ConvertToCharPtrPtr (std::vector<std::string> &argvs)
{
    size_t SIZE = argvs.size();
    char** argv = new char *[SIZE];
    for(int i = 0; i < SIZE; i++) {
        char *str = new char[argvs[static_cast<size_t>(i)].length() + 1];
        strcpy(str,  argvs[static_cast<size_t>(i)].c_str());
        argv[i] = str;
    }
    return const_cast<const char**>(argv);
}

// When Argc is 1
// Then don't run tests
TEST(ArgParserTests, WhenArgcIs1ThenDontRunTests) {

    std::vector<std::string> argv = { "Creator3D" };
    const char** argvs = ConvertToCharPtrPtr(argv);
    po::ArgParser* parser = new po::ArgParser(static_cast<int>(argv.size()), argvs);

    EXPECT_EQ(parser->HasFlag("-test"), false);

    delete [] argvs;
    delete parser;
}

// When Argc is 2 But its not "-test"
// Then don't run tests
TEST(ArgParserTests, WhenArgcIs2AndIsNotTestFlagThenDoNotRunTests) {
    std::vector<std::string> argv = { "Creator3D" , "-smile"};
    const char** argvs = ConvertToCharPtrPtr(argv);
    po::ArgParser* parser = new po::ArgParser(static_cast<int>(argv.size()), argvs);

    EXPECT_EQ(parser->HasFlag("-test"), false);

    delete [] argvs;
    delete parser;
}

// When Argc is 2 and have "-test" argv
// Then run tests
TEST(ArgParserTests, WhenArgcIs2AndHaveTestFlagThenDoRunTests) {
    std::vector<std::string> argv = { "Creator3D" , "-test"};
    const char** argvs = ConvertToCharPtrPtr(argv);
    po::ArgParser* parser = new po::ArgParser(static_cast<int>(argv.size()), argvs);

    EXPECT_EQ(parser->HasFlag("-test"), true);

    delete [] argvs;
    delete parser;
}

// When Argc is 5 But and have "-test" argv somewhere
// Then run tests
TEST(ArgParserTests, WhenArgcIs5AndHaveTestFlagSomewhereThenDoRunTests) {
    std::vector<std::string> argv = { "Creator3D" , "-smile", "-every", "-test", "-day" };
    const char** argvs = ConvertToCharPtrPtr(argv);
    po::ArgParser* parser = new po::ArgParser(static_cast<int>(argv.size()), argvs);

    EXPECT_EQ(parser->HasFlag("-test"), true);

    delete [] argvs;
    delete parser;
}

