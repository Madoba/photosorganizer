#include "Directory.hpp"

#include <filesystem>

namespace po
{
    Directory::Directory(const std::string& path)
        : path(path) 
    {}

    bool Directory::Exists()
    {
        return std::filesystem::exists(path) ? true : false;
    }

    bool Directory::CreaterFolder(const std::string& name)
    {
        std::filesystem::path dir(path + "/" + name);
        return std::filesystem::create_directory(dir);
    }

    std::vector<File> Directory::GetAllFiles()
    {
        std::vector<File> files;
        for (const auto & entry : std::filesystem::directory_iterator(path))
        {
            if(entry.path().extension() == ".jpg")
                files.push_back(File(entry.path().string()));
        }
        return files;
    }

    std::vector<JPGFile> Directory::GetAllJPGFiles()
    {
        std::vector<JPGFile> files;
        for (const auto & entry : std::filesystem::directory_iterator(path))
        {
            if(entry.path().extension() == ".jpg")
                files.push_back(JPGFile(entry.path().string()));
        }
        return files;
    }
}