#include "Date.hpp"

#include <ctime>
#include <sstream>
#include <iomanip>
#include <windows.h>
namespace po
{
    Date::Date()
    {
        time =  std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch());
    }

    Date::Date(const std::chrono::milliseconds& millisec)
        : time(millisec)
    {}

    // Accepted format "%Y:%m:%d %X"
    Date::Date(const std::string& date)
    {
        std::tm tm = {0};
        std::stringstream ss(date);
        ss >> std::get_time(&tm, "%Y:%m:%d %X");
        tm.tm_isdst = -1;
        time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::from_time_t(std::mktime(&tm)).time_since_epoch());
    }

    bool Date::After(const Date& date)
    {
        return GetTime() > date.GetTime() ? true : false;
    }

    bool Date::Before(const Date& date)
    {
        return GetTime() < date.GetTime() ? true : false;
    }

    bool Date::Equals(const Date& date)
    {
        return GetTime() == date.GetTime() ? true : false;
    }
    
    // Tolerance in milliseconds
    bool Date::Equals(const Date& date, const int& tolerance)
    {
        int diff = abs(GetTime() - date.GetTime());
        if(diff <= tolerance)
            return true;
        return false;
    }

    int Date::Year() const
    {
        std::chrono::system_clock::time_point tp{time};
        std::time_t time_c = std::chrono::system_clock::to_time_t(tp);
        std::tm timetm = *std::localtime(&time_c);
        char date[5];  
        strftime(date, sizeof(date), "%Y", &timetm);
        return std::atoi(date);
    }

    int Date::Month() const
    {
        std::chrono::system_clock::time_point tp{time};
        std::time_t time_c = std::chrono::system_clock::to_time_t(tp);
        std::tm timetm = *std::localtime(&time_c);
        char date[5];  
        strftime(date, sizeof(date), "%m", &timetm);
        return std::atoi(date);         
    }

    int Date::Day() const
    {
        std::chrono::system_clock::time_point tp{time};
        std::time_t time_c = std::chrono::system_clock::to_time_t(tp);
        std::tm timetm = *std::localtime(&time_c);
        char date[5];  
        strftime(date, sizeof(date), "%d", &timetm);
        return std::atoi(date); 
    }

    std::string Date::FormatMonthToPT()
    {
        switch(Month())
        {
            case 1:
                return "Janeiro";
            case 2:
                return "Fevereiro";
            case 3:
                return UTF_to_ASCII("Março"); // windows and weird symbols
            case 4:
                return "Abril";
            case 5:
                return "Maio";
            case 6:
                return "Junho";
            case 7:
                return "Julho";
            case 8:
                return "Agosto";
            case 9:
                return "Setembro";
            case 10:
                return "Outubro";
            case 11:
                return "Novembro";
            case 12:
                return "Dezembro";
            default:
                return std::string();
        }
        return std::string();
    }
    
    std::string Date::ToString()
    {
        std::chrono::system_clock::time_point tp{time};
        std::time_t time_c = std::chrono::system_clock::to_time_t(tp);
        std::tm timetm = *std::localtime(&time_c);
        char date[80];  
        strftime(date, sizeof(date), "%Y-%m-%d %X", &timetm);
        return date;     
    }

    long Date::GetTime() const
    {
        return time.count();
    }

    Date Date::Now()
    {
        return Date();
    }

    std::string Date::UTF_to_ASCII(const std::string& str)
    {
        int wchars_num = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, NULL, 0);
        wchar_t* wstr = new wchar_t[wchars_num];
        MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, wstr, wchars_num);
        std::wstring ws(wstr);
        std::string newStr = std::string(ws.begin(), ws.end());
        delete[] wstr;
        return newStr;
    }
}