#ifndef File_hpp
#define File_hpp

#include <string>
#include <fstream>

namespace po
{
    class File
    {
    public:
        File(const std::string& path);
        ~File() = default;

        bool Exists();

        std::string Extension() const;
        std::string Name() const;
        
        std::ifstream GetBinaryStream() const;

        bool Copy(const std::string& to);

    private:
        std::string path;
    };
}

#endif