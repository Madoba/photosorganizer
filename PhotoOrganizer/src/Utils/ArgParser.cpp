#include "ArgParser.hpp"

#include <iostream>

namespace po 
{
    ArgParser::ArgParser(int argc, const char** argv)
    {
        for(int i = 0; i < argc; i++)
            this->argv.push_back(argv[i]);
    }

    bool ArgParser::HasFlag(const std::string& flag)
    {
        if(argv.size() < 2 )
            return false;
        for(const auto& value : argv)
        {
            if(value == flag)
                return true;
        }
        return false;
    }

    bool ArgParser::IsHelp()
    {
        if(argv.size() != 2 )
            return false;
        if(argv[1] == "-help" || argv[1] == "-h")
            return true;
        return false;
    }

    bool ArgParser::IsTest()
    {
        if(argv.size() != 2 )
            return false;
        if(argv[1] == "-test" || argv[1] == "-t")
            return true;
        return false;
    }


    bool ArgParser::HasFlags()
    {
        return argv.size() > 1 ? true : false;
    }

    bool ArgParser::HasSourceDirectory()
    {
        return HasFlag("-s");
    }

    bool ArgParser::HasDestinationDirectory()
    {
        return HasFlag("-d");
    }

    bool ArgParser::HasInvalidFlags()
    {
        return HasFlags() ? false 
        : IsTest() ? false 
        : IsHelp() ? false
        : HasSourceDirectory() ? false
        : HasDestinationDirectory() ? false
        : true; 
    }

    std::string ArgParser::GetDestinationDirectory()
    {
        return GetValueAfterFlag("-d");
    }

    std::string ArgParser::GetSourceDirectory()
    {
        return GetValueAfterFlag("-s");
    }

    std::string ArgParser::GetValueAfterFlag(const std::string& flag)
    {
       for(int i = 0; i < argv.size() - 1; i++)
        {
            if(argv[i] == flag)
                return argv[i + 1];
        }
        return std::string();
    }
    
    ArgParser::~ArgParser()
    {
    }
}