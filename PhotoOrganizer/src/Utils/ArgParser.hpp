#ifndef ArgParser_hpp
#define ArgParser_hpp

#include <vector>
#include <string>

namespace po
{
    class ArgParser
    {
    public:
        ArgParser(int argc, const char** argv);
        ~ArgParser();

        bool HasFlag(const std::string& flag);
        bool IsTest();
        bool IsHelp();
        bool HasFlags();
        bool HasSourceDirectory();
        bool HasDestinationDirectory();
        bool HasInvalidFlags();

        std::string GetDestinationDirectory();
        std::string GetSourceDirectory();

protected:
        std::string GetValueAfterFlag(const std::string& flag);

    private:
        std::vector<std::string> argv;
    };
   
    
}

#endif