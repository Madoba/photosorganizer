#ifndef Date_hpp
#define Date_hpp

#include <string>
#include <chrono>

namespace po
{
    class Date
    {
    public:
        Date();
        Date(const std::chrono::milliseconds& millisec);
        // Accepted format "%Y:%m:%d %X"
        Date(const std::string& date);
        ~Date() = default;

        bool After(const Date& date);
        bool Before(const Date& date);
        bool Equals(const Date& date);
        // Tolerance in milliseconds
        bool Equals(const Date& date, const int& tolerance);

        int Year() const;
        int Month() const;
        int Day() const;

        std::string FormatMonthToPT();
        std::string ToString();

        long GetTime() const;

        static Date Now();

    private:
    // Windows
    std::string UTF_to_ASCII(const std::string& str);

    private:
        std::chrono::milliseconds time;
    };
}

#endif