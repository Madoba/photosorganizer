#ifndef Directory_hpp
#define Directory_hpp

#include <string>
#include <vector>

#include "File.hpp"
#include "JPGFile.hpp"

namespace po
{
    class Directory
    {
    public:
        Directory(const std::string& path);
        ~Directory() = default;

        bool Exists();

        bool CreaterFolder(const std::string& name = "");

        std::vector<File> GetAllFiles();
        std::vector<JPGFile> GetAllJPGFiles();


    private:
        std::string path;
    };
}

#endif