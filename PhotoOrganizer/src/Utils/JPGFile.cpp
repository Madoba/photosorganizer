#include "JPGFile.hpp"

#include "TinyEXIF.hpp"

#include <filesystem>
#include <iostream>
#include <string>

namespace po
{
    JPGFile::JPGFile(const std::string& path)
        : File(path)
    {
        if( Extension() == ".jpg")
            valid = true;
    }

    bool JPGFile::isValid() const
    {
        return valid;
    }

     Date JPGFile::GetCreationDate() const
     {
        std::ifstream stream = GetBinaryStream();
        if (!stream) {
		    std::cout << "Error: can not open '" << Name() << "'\n";
            return Date();
	    }
        TinyEXIF::EXIFInfo imageEXIF(stream);
        if (!imageEXIF.Fields) {
            std::cout << "Error: " << Name() << " has no EXIF or XMP metadata\n";
            return Date();
        }

	    if (!imageEXIF.DateTimeOriginal.empty())
        {
            return Date(imageEXIF.DateTimeOriginal);
        }
        return Date();
     }
}