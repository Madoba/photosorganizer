#ifndef JPGFile_hpp
#define JPGFile_hpp

#include "File.hpp"
#include "Date.hpp"

namespace po
{
    class JPGFile : public File 
    {
    public:
        JPGFile(const std::string& path);
        ~JPGFile() = default;

        bool isValid() const;

        Date GetCreationDate() const;

    private:
    bool valid = false;
    };
}

#endif