#include "File.hpp"

#include <filesystem>

namespace po
{
        File::File(const std::string& path)
            : path(path)
        {}

         bool File::Exists()
         {
              return std::filesystem::exists(path) ? true : false;
         }

        std::string File::Extension() const
        {
             std::filesystem::path filePath(path);
             return filePath.extension().string();

        }

        std::string File::Name() const
        {
            std::filesystem::path filePath(path);
            return filePath.filename().string();
        }

        std::ifstream File::GetBinaryStream() const
        {
            return std::ifstream(path, std::ios::binary);
        }

        bool File::Copy(const std::string& to)
        {
            if(!Exists())
                return false;

            std::ifstream src(path, std::ios::binary);
            std::ofstream dst(to,   std::ios::binary);
            dst << src.rdbuf();

            if(File(to).Exists())
                return true;
            return false;
        }
}