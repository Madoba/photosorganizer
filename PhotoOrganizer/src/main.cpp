#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wuseless-cast"
#include "gtest/gtest.h"
#pragma GCC diagnostic pop

#include "Utils/ArgParser.hpp"
#include "Utils/Directory.hpp"
#include "Utils/TinyEXIF.hpp"
#include "Utils/Date.hpp"

#include <filesystem>
#include <vector>
#include <iostream>
#include <fstream>


void PrintHelpMsg()
{
    std::cout << "Photo organizer" << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout << "\tOrganize all .jpg files in the current directory." << std::endl;
    std::cout << "\t-s\t\tDeclare source directory" << std::endl;
    std::cout << "\t-d\t\tDeclare destination directory" << std::endl;
    std::cout << "\t-h, -help\tHelp" << std::endl;
    std::cout << "\nDefault:" << std::endl;
    std::cout << "\tSource: current directory" << std::endl;
    std::cout << "\tDestination: ./sorted" << std::endl;
    std::cout << "\nExample:" << std::endl;
    std::cout << "\tPhotoOrganizer.exe -s photos -d sorted" << std::endl;
}

int main(int argc, const char** argv)
{
    po::ArgParser* parser = new po::ArgParser(argc, argv);
    if(parser->HasInvalidFlags())
    {
        std::cout << "Invalid flag!" << std::endl;
        std::cout << "PhotoOrganizer.exe -h to display help!" << std::endl;
        return 1;
    }

#ifdef PO_DEBUG
    if(parser->IsTest())
    {
           testing::InitGoogleTest();
            return RUN_ALL_TESTS();
    }
#endif

    if(parser->IsHelp())
    {
        PrintHelpMsg();
        return 0;
    }

    std::string destinationPath = "./sorted";
    if(parser->HasDestinationDirectory())
    {
        destinationPath = "./" + parser->GetDestinationDirectory();
    }
    
    std::string sourcePath = "./";
    if(parser->HasSourceDirectory())
    {
        sourcePath += parser->GetSourceDirectory();
    }
    
    po::Directory srcDir(sourcePath);
    po::Directory dstDir(destinationPath);

    if(!dstDir.Exists())
        dstDir.CreaterFolder();

    if(!srcDir.Exists())
    {
        std::cout << "Path " << sourcePath <<  " does not exist!";
        return 1;
    }

    for(auto& file : srcDir.GetAllJPGFiles())
    {
        std::cout << file.Name() << std::endl;
        po::Date date = file.GetCreationDate();

        if(date.Equals(po::Date::Now()))
        {
            file.Copy(destinationPath +  "/" + file.Name());
            continue; 
        }
        std::string yearDirectoryStr = destinationPath + "/" + std::to_string(date.Year());
        std::string monthDirectoryStr =  yearDirectoryStr + "/" + std::to_string(date.Month()) + " - " + date.FormatMonthToPT();
        po::Directory yearDir(yearDirectoryStr);
        po::Directory monthDir(monthDirectoryStr);
        if(!yearDir.Exists())
            yearDir.CreaterFolder();
        if(!monthDir.Exists())
            monthDir.CreaterFolder();
            
        file.Copy(monthDirectoryStr + "/" + file.Name());
    }
    return 0;
}
