@echo off
pushd %~dp0\..\
call mingw32-make config=debug MATCHCOMPILER=yes FILESDIR=/usr/share/cppcheck HAVE_RULES=yes CXXFLAGS="-O2 -DEBUG -Wall -Wno-sign-compare -Wnon-virtual-dtor -Wold-style-cast -Wno-unused-function -Wunused -Wextra -Wconversion -Wnon-virtual-dtor -pedantic -Wcast-align -Woverloaded-virtual -Wpedantic -Wsign-conversion -Wmisleading-indentation -Wduplicated-cond -Wduplicated-branches -Wlogical-op -Wnull-dereference -Wuseless-cast -Wdouble-promotion -Wformat=2"
